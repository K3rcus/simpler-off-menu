# Simpler Off Menu

This extension removes the Power Off/Logout submenu and  allow you to choose what do you want to have in the Power Off menu.
  
You can choose to show Lock, Settings, Logout, Gnome-Tweaks, Suspend and Hibernate.  
  
Also allows you to merge items:  
Gnome-Tweaks in the Settings button, use left-click to launch Settings and right-click to launch Gnome-Tweaks.  
Logout in the Power Off button, use left-click to PowerOff and right-click to Logout  
  
**After changing the settings, disable and re-enable the extension for the change to take effect.**  
  
Feedback welcome!
  

![Simpler Off Menu](https://gitlab.com/K3rcus/simpler-off-menu/-/raw/master/SOMv5.png)
  
